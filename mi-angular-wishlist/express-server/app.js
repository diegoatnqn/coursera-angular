var express = require('express')
var cors = require('cors')
var app = express()

app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var comidas = ["Pastafrola", "Tortas Fritas", "Biscochitos", "Tostadas","Torta selva negra","Medialunas","Medialunas saladas","Grisines","Grisines dulces","Facturas","Factura cara sucia"];
app.get("/comidas", (req, res, next) => res.json(comidas.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misComidas = [];
app.get("/my", (req, res, next) => res.json(misComidas));
app.post("/my", (req, res, next) => {
  console.log("TEST TEST POST APPJS");
  console.log(req.body);
  misComidas.push(req.body.nuevo);
  res.json(misComidas);
});

app.get("/api/translation", (req, res, next) => res.json([
  { lang: req.query.lang, key: 'HOLA', value: 'HOLA' + req.query.lang }
]));


