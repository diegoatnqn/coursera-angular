import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import "@angular/compiler";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { OpcionesComidaComponent } from './components/opciones-comida/opciones-comida.component';
import { ListaComidasComponent } from './components/lista-comidas/lista-comidas.component';
import { ComidaDetalleComponent } from './components/comida-detalle/comida-detalle.component';
import { FormOpcionComidaComponent } from './components/form-opcion-comida/form-opcion-comida.component';
import Dexie from 'dexie';
import { OpcionComidaState, reducerOpcionComidas, intializeOpcionComidaState, opcionComidaEffects, InitMyDataAction } from './models/opciones-comidas-state.model';
import { StoreModule as ngrxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { RestaurantesComponent } from './components/restaurantes/restaurantes-component/restaurantes-component.component';
import { RestaurantesMainComponent } from './components/restaurantes/restaurantes-main-component/restaurantes-main-component.component';
import { RestaurantesMasInfoComponent } from './components/restaurantes/restaurantes-mas-info-component/restaurantes-mas-info-component.component';
import { RestaurantesDetalleComponent} from './components/restaurantes/restaurantes-detalle-component/restaurantes-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http'
import { opcionComida } from './models/opcion-comida.model';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';


//app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//fin app config

//DEXIE DB
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn:'root'
})
export class MyDatabase extends Dexie{
  comidas: Dexie.Table<opcionComida, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      comidas: '++id,nombre,imagenUrl',
    });
    this.version(2).stores({
      comidas: '++id,nombre,imagenUrl',
      translations: '++id,lang,key,value'
    });
  }
}
export const db = new MyDatabase();
//FIN DEXIE

//i18n init
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
      .where('lang')
      .equals(lang)
      .toArray()
      .then(results => {
        if (results.length === 0) {
          return this.http
            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
            .toPromise()
            .then(apiResults => {
              db.translations.bulkAdd(apiResults);
              return apiResults;
            });
        }
        return results;
      }).then((traducciones) => {
        console.log('traducciones cargadas:');
        return traducciones;
      }).then((traducciones) => {
        return traducciones.map((t) => ({ [t.key]: t.value }));
      });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}
function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
//FIN i18n

//inicio routing
export const childrenRoutesRestaurantes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: RestaurantesMainComponent },
  { path: 'mas-info', component: RestaurantesMasInfoComponent },
  { path: ':id', component: RestaurantesDetalleComponent },
  
]
  ;
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaComidasComponent },
  { path: 'comida/:id', component: ComidaDetalleComponent},
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [UsuarioLogueadoGuard] },
  {
    path: 'restaurantes',
    component: RestaurantesComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesRestaurantes
  }
];
//Fin routing

//redux init
export interface AppState {
  comidas: OpcionComidaState;
}
const reducers: ActionReducerMap<AppState> = {
  comidas: reducerOpcionComidas
}
const reducersInitialState = {
  comidas: intializeOpcionComidaState()
}
//fin redux

//app init
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeOpcionComidasState();
}
@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async initializeOpcionComidasState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
    console.log("APP LOAD SERVICE ANDANDO");
  }
}
//Fin app init
@NgModule({
  declarations: [
    AppComponent,
    OpcionesComidaComponent,
    ListaComidasComponent,
    FormOpcionComidaComponent,
    LoginComponent,
    ProtectedComponent,
    RestaurantesComponent,
    RestaurantesMainComponent,
    RestaurantesMasInfoComponent,
    RestaurantesDetalleComponent,
    ComidaDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
    ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    ngrxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability:false
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([opcionComidaEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ], 
  providers: [
    AuthService, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ], schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
