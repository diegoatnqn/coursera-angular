import { v4 as uuid } from 'uuid';
export class opcionComida
{
  public selected: boolean;
  public sabores: string[];
  id = uuid();
    public votes: number = 0;
  //constructor
  constructor(public nombre: string, public url: string) {
    this.sabores = ["dulce", "crema"];
  }
  //metodos
  isSelected(): boolean {
    return this.selected;
  }
    setSelected(s: boolean) {
      this.selected = s;
    }

    voteUp() {
      this.votes++;
    }
    voteDown() {
      this.votes--;
    }
    reiniciarVotos() {
      this.votes = 0;
    }
}
