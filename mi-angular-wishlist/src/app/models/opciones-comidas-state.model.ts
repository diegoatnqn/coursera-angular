import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { opcionComida } from './opcion-comida.model';
import { ComidasApiClient } from './comidas-api-client.model';
import { HttpHeaders, HttpRequest, HttpResponse, HttpClientModule } from '@angular/common/http';

// ESTADO
export interface OpcionComidaState {
  items: opcionComida[];
  loading: boolean;
  favorito: opcionComida;
}

export function intializeOpcionComidaState() {
  return {
    items: [],
    loading: false,
    favorito: null
  };
}

// ACCIONES
export enum opcionComidaActionTypes {
  NUEVO_DESTINO = '[Opcion Comida] Nuevo',
  ELEGIDO_FAVORITO = '[Opcion Comida] Favorito',
  VOTE_UP = '[Opcion Comida] Vote Up',
  VOTE_DOWN = '[Opcion Comida] Vote Down',
  INIT_MY_DATA = '[Opcion Comida] Init My Data',
  REINICIO = '[Opcion Comida] Reinicio Votos',
  VACIAR = '[Opcion Comida] Vaciar Lista'
}

export class opcionComidaAction implements Action {
  type = opcionComidaActionTypes.NUEVO_DESTINO;
  constructor(public destino: opcionComida) { }
}

export class ElegidoFavoritoAction implements Action {
  type = opcionComidaActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: opcionComida) { }
}

export class VoteUpAction implements Action {
  type = opcionComidaActionTypes.VOTE_UP;
  constructor(public destino: opcionComida) { }
}

export class VoteDownAction implements Action {
  type = opcionComidaActionTypes.VOTE_DOWN;
  constructor(public destino: opcionComida) { }
}

export class InitMyDataAction implements Action {
  type = opcionComidaActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) { }
}
export class reinicioAction implements Action {
  type = opcionComidaActionTypes.REINICIO;
  constructor() { }
}
export class vaciarAction implements Action {
  type = opcionComidaActionTypes.VACIAR;
  constructor() { }
}

export type DestinosViajesActions = opcionComidaAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | InitMyDataAction | reinicioAction;

                                              // REDUCERS
export function reducerOpcionComidas(
  state: OpcionComidaState,
  action: DestinosViajesActions
): OpcionComidaState {
  switch (action.type) {
    case opcionComidaActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as unknown as InitMyDataAction).destinos;
      return {
        ...state,
        items: destinos.map((d) => new opcionComida(d, ''))
      };
    }
    case opcionComidaActionTypes.NUEVO_DESTINO: {
      console.log("NUEVO DESTINO ACTION en STATE MODEL");
      return {
        ...state,
        items: [...state.items, (action as opcionComidaAction).destino]
      };
    }
    case opcionComidaActionTypes.ELEGIDO_FAVORITO: {
      state.items.forEach(x => x.setSelected(false));
      const fav: opcionComida = (action as ElegidoFavoritoAction).destino;
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav
      };
    }
    case opcionComidaActionTypes.VOTE_UP: {
      const d: opcionComida = (action as VoteUpAction).destino;
      d.voteUp();
      return { ...state };
    }
    case opcionComidaActionTypes.VOTE_DOWN: {
      const d: opcionComida = (action as VoteDownAction).destino;
      d.voteDown();
      return { ...state };
    }
    case opcionComidaActionTypes.REINICIO: {
      state.items.forEach(x => x.reiniciarVotos());
      return { ...state };
    }
    case opcionComidaActionTypes.VACIAR: {
      state.items = [];
      return { ...state };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class opcionComidaEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(opcionComidaActionTypes.NUEVO_DESTINO),
    map((action: opcionComidaAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) { }
}
