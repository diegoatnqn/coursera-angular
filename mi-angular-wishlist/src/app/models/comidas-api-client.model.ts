import { opcionComida } from './opcion-comida.model';
import { Subject, BehaviorSubject, from } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { Store } from '@ngrx/store'
import { opcionComidaAction, ElegidoFavoritoAction } from './opciones-comidas-state.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class ComidasApiClient {
  comidas: opcionComida[] = [];

  //Constructor
  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient) {
        this.store.select(state => state.comidas).subscribe((data) => {
            console.log(data);
          this.comidas = data.items;
        });
        this.store.subscribe((data) => {
        });
  }
  add(d: opcionComida){
    console.log("ADD COMIDAS API CLIENT");
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new opcionComidaAction(d));
        const myDb = db;
        myDb.comidas.add(d);
        console.log("Todas las comidas de la db");
        myDb.comidas.toArray().then(comidas => console.log(comidas))
      }
    });
  }
  getAll() {
    return this.comidas
  }
  getById(id: string): opcionComida {
    return this.comidas.filter(function (d) {
      return d.id.toString() == id;
    })[0];
  } 
  elegir(d: opcionComida) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
