import { reducerOpcionComidas, OpcionComidaState, intializeOpcionComidaState, InitMyDataAction, opcionComidaAction } from './opciones-comidas-state.model'
import { opcionComida } from './opcion-comida.model';

describe('reducerOpcionComidas', () => {
  it('should reduce init data', () => {
    //setup
    const prevState: OpcionComidaState = intializeOpcionComidaState();
    const action: InitMyDataAction = new InitMyDataAction(['comida 1', 'comida 2']);
    //action
    const newState: OpcionComidaState = reducerOpcionComidas(prevState, action);
    //assert
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('comida 1');
  });

  it('should reduce new item added', () => {
    const prevState: OpcionComidaState = intializeOpcionComidaState();
    const action: opcionComidaAction = new opcionComidaAction(new opcionComida('biscochitos', 'url'));
    const newState: OpcionComidaState = reducerOpcionComidas(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('biscochitos');
  });
});
