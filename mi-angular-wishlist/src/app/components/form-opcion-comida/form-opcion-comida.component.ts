import { Component, OnInit, Output, EventEmitter, Input, Inject, forwardRef } from '@angular/core';
import { opcionComida } from '../../models/opcion-comida.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators'
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from '../../app.module';

@Component({
  selector: 'app-form-opcion-comida',
  templateUrl: './form-opcion-comida.component.html',
  styleUrls: ['./form-opcion-comida.component.css']
})
export class FormOpcionComidaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<opcionComida>;
  fg: FormGroup;
  longitud: number = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();

    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.longitud)])],
      url: ['']
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log("Cambio el formulario" ,form);
    })
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/comidas?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response); 
  }

  guardar(nombre: string, url: string): boolean {
    const d = new opcionComida(nombre, url);
    console.log("GUARDAR ANTES DE EMIT");
    this.onItemAdded.emit(d);

    console.log("GUARDAR DESPUES DE EMIT");
    return false;
  }
  nombreValidator(control: FormControl): { [s: string]: boolean } {
    let l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidNombre: true }
    } else {
      return null;
    }
  }
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      let l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true }
      } else {
        return null;
      }
    }
  }
  search
}
