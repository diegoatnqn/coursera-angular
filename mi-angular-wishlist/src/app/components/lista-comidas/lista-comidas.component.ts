import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { opcionComida } from './../../models/opcion-comida.model';
import { ComidasApiClient } from './../../models/comidas-api-client.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, opcionComidaAction, reinicioAction, vaciarAction } from '../../models/opciones-comidas-state.model';

@Component({
  selector: 'app-lista-comidas',
  templateUrl: './lista-comidas.component.html',
  styleUrls: ['./lista-comidas.component.css'],
  providers: [ComidasApiClient]
})
export class ListaComidasComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<opcionComida>;
  updates: string[];
  all;
  //destinos: DestinoViaje[];
  constructor(private comidasApiClient: ComidasApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    
    this.store.select(state => state.comidas.favorito).subscribe
      (data => {
        const fav = data;
        if (data != null) {
          this.updates.push('Se ha elegido a ' + data.nombre);
        }
      });
    store.select(state => state.comidas.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }
  agregado(d: opcionComida) {
    this.comidasApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  elegido(d: opcionComida) {
    this.comidasApiClient.elegir(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  reiniciarVotos() {
    this.store.dispatch(new reinicioAction());
  }
  vaciarLista() {
    this.store.dispatch(new vaciarAction());
  }
  }

