import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-restaurantes-detalle-component',
  templateUrl: './restaurantes-detalle-component.component.html',
  styleUrls: ['./restaurantes-detalle-component.component.css']
})
export class RestaurantesDetalleComponent implements OnInit {
  id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit(): void {
  }

}
