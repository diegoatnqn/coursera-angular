import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ComidasApiClient } from './../../models/comidas-api-client.model';
import { opcionComida } from './../../models/opcion-comida.model';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
@Component({
  selector: 'app-comida-detalle',
  templateUrl: './comida-detalle.component.html',
  styleUrls: ['./comida-detalle.component.css'],
  providers: [ComidasApiClient]
})

export class ComidaDetalleComponent implements OnInit {
  comida: opcionComida;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private comidasApiClient: ComidasApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.comida = this.comidasApiClient.getById(id);
  }
  
}
