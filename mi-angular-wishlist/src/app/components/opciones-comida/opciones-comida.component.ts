import { Component, OnInit, Input, HostBinding, EventEmitter,Output } from '@angular/core';
import { opcionComida } from './../../models/opcion-comida.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store'
import { VoteDownAction, VoteUpAction, reinicioAction } from '../../models/opciones-comidas-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-opciones-comida',
  templateUrl: './opciones-comida.component.html',
  styleUrls: ['./opciones-comida.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor:'WhiteSmoke'
      })),
      transition('estadoNoFavorito=>estadoFavorito', [animate('3s')]),
      transition('estadoFavorito=>estadoNoFavorito', [animate('1s')]),
    ])
  ]
})
export class OpcionesComidaComponent implements OnInit {
  @Input() comida: opcionComida;
  @HostBinding('attr.class') cssClass = "col-md-4";
  @Output() clicked: EventEmitter<opcionComida>;
  @Input('idx') posicion: number;

  constructor(public store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
  ir() {
    this.clicked.emit(this.comida);
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.comida));
    return false
  }
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.comida));
    return false;
  }
  getAll() {

  }
}
