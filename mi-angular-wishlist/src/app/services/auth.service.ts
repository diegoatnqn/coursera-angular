import { Injectable } from '@angular/core';
import { Local } from 'protractor/built/driverProviders';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, pass: string): boolean {
    let activo: boolean;
    if (user === "user" && pass === "user") {
      localStorage.setItem("username", user);
      activo = true;
    } else {
      activo = false;
    }
    return activo;
  }
  logout(): any {
    localStorage.removeItem("username");
    console.log("LOGOUT AUTH SERVICE");
  }
  getUser(): any {
    return localStorage.getItem("username");
    
  }
  isLoggedIn(): boolean {
    console.log("ISLOGGED IN AUTHSEVICE");
    return this.getUser() !== null;
    
  }
}
