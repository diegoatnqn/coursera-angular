describe('ventana principal', () => {
  it('tiene encabezado correcto y en español por defecto', () => {
    cy.visit('http://localhost:4200');
    cy.contains('mi-angular-wishlist');
    cy.get('p b').should('contain', 'HOLAes');
    cy.get('h5').should('contain', 'Actividad');
    cy.get('button').should('contain', 'Reiniciar');
    cy.get('#nombre').type('TESTING MAIN');
  });
});
